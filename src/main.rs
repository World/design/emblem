// SPDX-License-Identifier: GPL-3.0-or-later
#![forbid(unsafe_code)]
mod application;
#[rustfmt::skip]
mod config;
mod color_button;
mod color_widget;
mod export;
mod generate;
mod image;
mod svg_paintable;
mod utils;
mod window;

pub use color_button::ColorButton;
pub use color_widget::ColorWidget;
pub use export::Export;
pub use generate::Generate;
pub use svg_paintable::SvgPaintable;
pub use window::Window;

use application::Application;
use config::{GETTEXT_PACKAGE, LOCALEDIR, RESOURCES_FILE};
use gettextrs::{gettext, LocaleCategory};
use gtk::{gio, glib};

fn main() -> glib::ExitCode {
    // Initialize logger
    env_logger::init();

    // Prepare i18n
    gettextrs::setlocale(LocaleCategory::LcAll, "");
    gettextrs::bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR).expect("Unable to bind the text domain");
    gettextrs::textdomain(GETTEXT_PACKAGE).expect("Unable to switch to the text domain");

    glib::set_application_name(&gettext("Emblem"));

    let res = gio::Resource::load(RESOURCES_FILE).expect("Could not load gresource file");
    gio::resources_register(&res);

    let app = Application::new();

    app.run()
}
